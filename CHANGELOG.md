The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.5.0] - 2020-10-22
### Added
* `/user` call
* `/format` call
* `/list-orders` call
* `/order-status` call
* `/order` POST call
* `/order` GET call (retrieves order details)
* `/item-status` call
* `/available-products` call
