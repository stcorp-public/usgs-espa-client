# Usgs ESPA API client

An API for using the [USGS ESPA](https://espa.cr.usgs.gov/static/docs/api-resources-list.html) data ordering and download interface.

## Basic Usage
```rust
use usgs_espa_client::{Client, Result};
use usgs_espa_client::types::{Credentials};

#[tokio::main]
async fn main() -> Result<()> {
  let credentials = Credentials::from_env()?;
  let client = Client::new(credentials)?;
  let orders = client.list_orders().await?;
  Ok(())
}
```

![](st_logo.svg)

Made by: [S&T Norway](https://www.stcorp.no)
