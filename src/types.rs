//! Types for working with the API

use std::collections::HashMap;
use std::env::var;
use std::fmt;
use std::str::FromStr;

use serde::{Deserialize, Serialize};
use serde_json::to_string_pretty;

use crate::constants::{ENVVAR_PASS, ENVVAR_USER};
use crate::error::Error;
use crate::Result;

#[derive(Clone, Debug, Serialize, Deserialize)]
/// USGS ERS access credentials
pub struct Credentials {
    pub username: String,
    pub password: String,
}

impl Credentials {
    /// Read the access credentials from [environment variables](index.html#constants)
    pub fn from_env() -> Result<Credentials> {
        let username =
            var(ENVVAR_USER).map_err(|_| Error::EnvironmentError(ENVVAR_USER.to_string()))?;
        let password =
            var(ENVVAR_PASS).map_err(|_| Error::EnvironmentError(ENVVAR_PASS.to_string()))?;

        Ok(Credentials { username, password })
    }

    /// Convience constructor method
    ///
    /// # Example
    ///
    /// ```
    /// # use usgs_espa_client::{Client, Result};
    /// # use usgs_espa_client::types::Credentials;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///   let credentials = Credentials::new("alice_p_hacker", "pass123");
    /// #     Ok(())
    /// # }
    pub fn new(username: &str, password: &str) -> Credentials {
        Credentials {
            username: username.to_owned(),
            password: password.to_owned(),
        }
    }
}

/// General API response type that includes all implemented responses
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiResponse {
    User(User),
    OrderList(Vec<String>),
    OrderDetails(OrderDetails),
    OrderStatus(OrderStatus),
    ItemStatus(ItemStatus),
    AvailableProducts(AvailableProducts),
    Formats(Formats),
}

/// User information
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct User {
    pub email: String,
    pub first_name: String,
    pub last_name: String,
    pub roles: Vec<String>,
    pub username: String,
}

/// Order status
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OrderStatus {
    pub orderid: String,
    pub status: OrderStatusState,
}

/// Possible order status states
#[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum OrderStatusState {
    Ordered,
    Complete,
    Purged,
}

impl FromStr for OrderStatusState {
    type Err = Error;
    /// Convert a &str to an order status state
    fn from_str(s: &str) -> Result<OrderStatusState> {
        let state = match s {
            "Ordered" => OrderStatusState::Ordered,
            "ordered" => OrderStatusState::Ordered,
            "Complete" => OrderStatusState::Complete,
            "complete" => OrderStatusState::Complete,
            "Purged" => OrderStatusState::Purged,
            "purged" => OrderStatusState::Purged,
            _ => {
                let message = String::from(format!("Unknown order status kind: {}", s));
                return Err(Error::ValidationError(message));
            }
        };
        Ok(state)
    }
}

/// Order details
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OrderDetails {
    pub completion_date: String,
    pub note: Option<String>,
    pub order_date: String,
    pub order_source: String,
    pub order_type: String,
    pub orderid: String,
    pub priority: String,
    pub product_options: String,
    pub product_opts: HashMap<String, ProductOpt>,
    pub status: OrderStatusState,
}

/// Dataset format
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Format {
    pub format: FormatKind,
}

/// Possible dataset format kinds
#[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "kebab-case")]
pub enum FormatKind {
    Gtiff,
    Envi,
    HdfEos2,
    Netcdf,
}

impl FromStr for FormatKind {
    type Err = Error;
    /// Convert a &str to a dataset format kind
    fn from_str(s: &str) -> Result<FormatKind> {
        let kind = match s {
            "gtiff" => FormatKind::Gtiff,
            "envi" => FormatKind::Envi,
            "hdf-eos2" => FormatKind::HdfEos2,
            "netcdf" => FormatKind::Netcdf,
            _ => {
                let message = String::from(format!("Unknown format kind: {}", s));
                return Err(Error::ValidationError(message));
            }
        };
        Ok(kind)
    }
}

/// `formats` GET endpoint response type
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Formats {
    pub formats: HashMap<String, String>,
}

/// Order definition
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct OrderDefinition {
    pub inputs: Vec<String>,
    pub products: Vec<String>,
}

impl fmt::Display for OrderDefinition {
    /// Provide Display trait implementation for OrderDefinition
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pretty_str = to_string_pretty(self);
        match pretty_str {
            Ok(value) => return write!(f, "{}", value),
            Err(_) => return Err(fmt::Error),
        }
    }
}

/// A `product_opt` as returned by the `order-details` API endpoint.
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ProductOpt {
    OrderDefinition(OrderDefinition),
    Format(FormatKind),
}

impl fmt::Display for ProductOpt {
    /// Provide Display trait implementation
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pretty_str = to_string_pretty(self);
        match pretty_str {
            Ok(value) => return write!(f, "{}", value),
            Err(_) => return Err(fmt::Error),
        }
    }
}

/// An item of `available-products` endpoint response
#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(untagged)]
pub enum AvailableProductItem {
    OrderDefinition(OrderDefinition),
    Scenes(Vec<String>),
}

impl fmt::Display for AvailableProductItem {
    /// Implement the Display trait for AvailableProductItem
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let pretty_str = to_string_pretty(self);
        match pretty_str {
            Ok(value) => return write!(f, "{}", value),
            Err(_) => return Err(fmt::Error),
        }
    }
}

/// `available-products` response
pub type AvailableProducts = HashMap<String, AvailableProductItem>;

/// Order item status
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SingleItemStatus {
    pub cksum_download_url: String,
    pub completion_date: String,
    pub name: String,
    pub note: String,
    pub product_dload_url: String,
    pub status: OrderStatusState,
}

/// `item-status` response
pub type ItemStatus = HashMap<String, Vec<SingleItemStatus>>;

/// Input to `available-products` endpoint request
#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AvailableProductsRequest {
    pub inputs: Vec<String>,
}

/// POST `order` endpoint request
pub type PostOrderRequest = HashMap<String, ProductOpt>;
