//! An API client for [USGS ESPA API](https://espa.cr.usgs.gov/static/docs/api-resources-list.html)
//!
//! For implemented API endpoints, see the methods implemented on the [Client](struct.Client.html#impl)

mod client;
mod constants;
mod error;

pub mod types;

pub use client::Client;
pub use error::{Error, Result};
// Make sure the relevant consts appear in documentation
pub use constants::{ENVVAR_PASS, ENVVAR_USER, ESPA_API_URL};
