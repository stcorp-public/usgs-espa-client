use reqwest::{Client as HttpClient, Url};
use serde::ser::Serialize;
use serde_json::{from_str, to_string};

// The import is actually used, the warning seems to be generated simply
// because the use is behind a compile flag
#[allow(unused_imports)]
use crate::constants::ESPA_API_URL;

use crate::constants::USERAGENT;
use crate::error::{Error, Result};
use crate::types::{
    ApiResponse, AvailableProducts, AvailableProductsRequest, Credentials, Formats, ItemStatus,
    OrderDetails, OrderStatus, PostOrderRequest, User,
};

#[cfg(test)]
use mockito;

/// USGS ESPA API client. This is the main interaction point with the API. The instantiation needs valid
/// user credentials that are used for authentication on each call.
///
/// Each method corresponds to an API call.
#[derive(Clone, Debug)]
pub struct Client {
    client: HttpClient,
    credentials: Credentials,
    base_url: Url,
}

impl Client {
    /// Instantiate a client with the given credentials
    ///
    /// # Example
    ///
    /// ```no_run
    /// # use usgs_espa_client::{Client, Result};
    /// use usgs_espa_client::types::Credentials;
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    ///    let credentials = Credentials::from_env()?;
    ///    let client = Client::new(credentials)?;
    /// #     Ok(())
    /// # }
    /// ```
    pub fn new(credentials: Credentials) -> Result<Client> {
        // Log in and get a session token
        #[cfg(not(test))]
        let base_url = Url::parse(ESPA_API_URL)?;
        #[cfg(test)]
        let base_url = Url::parse(&mockito::server_url())?;

        let client = HttpClient::builder().user_agent(USERAGENT).build()?;

        Ok(Client {
            client,
            base_url,
            credentials,
        })
    }

    /// Get the user information of the currently logged in user
    ///
    /// # Example
    /// ```no_run
    /// # use usgs_espa_client::{Client, Result};
    /// # use usgs_espa_client::types::{Credentials};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(credentials)?;
    ///    let user = client.user().await?;
    /// # Ok(())
    /// # }
    /// ```
    pub async fn user(&self) -> Result<User> {
        let endpoint = "user";
        let response = self.get(endpoint, None::<&()>).await?;
        let str_response = to_string(&response)?;
        match response {
            ApiResponse::User(user) => Ok(user),
            _ => Err(Error::UnexpectedResponse {
                response: str_response,
                url: endpoint.to_owned(),
                source: None,
            }),
        }
    }

    /// Get available formats
    ///
    /// # Example
    /// ```no_run
    /// # use usgs_espa_client::{Client, Result};
    /// # use usgs_espa_client::types::{Credentials};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(credentials)?;
    ///    let formats = client.formats().await?;
    /// # Ok(())
    /// # }
    /// ```
    pub async fn formats(&self) -> Result<Formats> {
        let endpoint = "formats";
        let response = self.get(endpoint, None::<&()>).await?;
        let str_response = to_string(&response)?;
        match response {
            ApiResponse::Formats(formats) => Ok(formats),
            _ => Err(Error::UnexpectedResponse {
                response: str_response,
                url: endpoint.to_owned(),
                source: None,
            }),
        }
    }

    /// List orders of the currently logged in user
    ///
    /// # Example
    /// ```no_run
    /// # use usgs_espa_client::{Client, Result};
    /// # use usgs_espa_client::types::{Credentials};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(credentials)?;
    ///    let orders = client.list_orders().await?;
    /// # Ok(())
    /// # }
    /// ```
    pub async fn list_orders(&self) -> Result<Vec<String>> {
        let endpoint = "list-orders";
        let response = self.get(endpoint, None::<&()>).await?;
        let str_response = to_string(&response)?;
        match response {
            ApiResponse::OrderList(orders) => Ok(orders),
            _ => Err(Error::UnexpectedResponse {
                response: str_response,
                url: endpoint.to_owned(),
                source: None,
            }),
        }
    }

    /// Get status of the given order
    ///
    /// # Example
    /// ```no_run
    /// # use usgs_espa_client::{Client, Result};
    /// # use usgs_espa_client::types::{Credentials};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(credentials)?;
    ///    let status = client.order_status("order_id").await?;
    /// # Ok(())
    /// # }
    /// ```
    pub async fn order_status(&self, order: &str) -> Result<OrderStatus> {
        let endpoint = "order-status".to_owned() + "/" + order;
        let response = self.get(&endpoint, None::<&()>).await?;
        let str_response = to_string(&response)?;
        match response {
            ApiResponse::OrderStatus(status) => Ok(status),
            _ => Err(Error::UnexpectedResponse {
                response: str_response,
                url: endpoint.to_owned(),
                source: None,
            }),
        }
    }

    /// Post an order
    ///
    /// # Example
    /// ```no_run
    /// # use std::collections::HashMap;
    /// # use usgs_espa_client::{Client, Result};
    /// use usgs_espa_client::types::{Credentials, ProductOpt, OrderDefinition, FormatKind};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(credentials)?;
    ///    let products = vec!["sr".to_owned()];
    ///    let scenes_l8 = vec![
    ///         "LC08_L1TP_197018_20181023_20181031_01_T1".to_owned(),
    ///         "LC08_L1TP_197019_20181023_20181031_01_T1".to_owned(),
    ///         "LC08_L1TP_197018_20181007_20181029_01_T1".to_owned(),
    ///     ];
    ///    let scenes_l7 = vec![
    ///         "LE07_L1TP_196019_20181024_20181119_01_T1".to_owned(),
    ///         "LE07_L1TP_198018_20181022_20181119_01_T1".to_owned(),
    ///         "LE07_L1TP_198019_20181022_20181119_01_T1".to_owned(),
    ///     ];
    ///    let mut order_request: HashMap<String, ProductOpt> = HashMap::new();
    ///    order_request.insert("olitirs8_collection".to_owned(), ProductOpt::OrderDefinition(OrderDefinition {
    ///                 inputs: scenes_l8,
    ///                 products: products.clone(),
    /// }));
    ///    order_request.insert("etm7_collection".to_owned(), ProductOpt::OrderDefinition(OrderDefinition {
    ///                 inputs: scenes_l7,
    ///                 products: products.clone(),
    /// }));
    ///     order_request.insert("format".to_owned(), ProductOpt::Format(FormatKind::Gtiff));
    ///
    ///    let post_reponse = client.post_order(order_request).await?;
    /// # Ok(())
    /// # }
    /// ```
    pub async fn post_order(&self, order: PostOrderRequest) -> Result<OrderStatus> {
        let endpoint = "order";
        let response = self.post(&endpoint, Some(&order)).await?;
        let str_response = to_string(&response)?;
        match response {
            ApiResponse::OrderStatus(status) => Ok(status),
            _ => Err(Error::UnexpectedResponse {
                response: str_response,
                url: endpoint.to_owned(),
                source: None,
            }),
        }
    }

    /// Get details of the given order
    ///
    /// # Example
    /// ```no_run
    /// # use usgs_espa_client::{Client, Result};
    /// # use usgs_espa_client::types::{Credentials};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(credentials)?;
    ///    let details = client.get_order("order_id").await?;
    /// # Ok(())
    /// # }
    /// ```
    pub async fn get_order(&self, order: &str) -> Result<OrderDetails> {
        let endpoint = "order".to_owned() + "/" + order;
        let response = self.get(&endpoint, None::<&()>).await?;
        let str_response = to_string(&response)?;
        match response {
            ApiResponse::OrderDetails(details) => Ok(details),
            _ => Err(Error::UnexpectedResponse {
                response: str_response,
                url: endpoint.to_owned(),
                source: None,
            }),
        }
    }

    /// Get order item status
    ///
    /// # Example
    /// ```no_run
    /// # use usgs_espa_client::{Client, Result};
    /// # use usgs_espa_client::types::{Credentials};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(credentials)?;
    ///    let details = client.item_status("order_id", None).await?;
    /// # Ok(())
    /// # }
    /// ```
    pub async fn item_status(&self, order: &str, scene: Option<&str>) -> Result<ItemStatus> {
        let endpoint = "item-status".to_owned() + "/" + order;

        let endpoint = match scene {
            Some(value) => endpoint + "/" + value,
            None => endpoint,
        };

        let response = self.get(&endpoint, None::<&()>).await?;
        let str_response = to_string(&response)?;
        match response {
            ApiResponse::ItemStatus(status) => Ok(status),
            _ => Err(Error::UnexpectedResponse {
                response: str_response,
                url: endpoint.to_owned(),
                source: None,
            }),
        }
    }

    /// Get available products for the given scenes
    ///
    /// # Example
    /// ```no_run
    /// # use usgs_espa_client::{Client, Result};
    /// # use usgs_espa_client::types::{Credentials};
    ///
    /// # #[tokio::main]
    /// # async fn main() -> Result<()> {
    /// #  let credentials = Credentials::from_env()?;
    /// #  let client = Client::new(credentials)?;
    ///    let scenes = vec!["scene1".to_owned(), "scene2".to_owned(), "scene3".to_owned()];
    ///    let details = client.available_products(scenes).await?;
    /// # Ok(())
    /// # }
    /// ```
    pub async fn available_products(&self, scenes: Vec<String>) -> Result<AvailableProducts> {
        let input_scenes = AvailableProductsRequest { inputs: scenes };
        let (endpoint, request) = match input_scenes.inputs.len() {
            0 => {
                return Err(Error::ValidationError(
                    "At least one scene ID expected".to_owned(),
                ))
            }
            1 => {
                let endpoint = "available-products".to_owned() + "/" + &input_scenes.inputs[0];
                let request = None;
                (endpoint, request)
            }
            _ => {
                let endpoint = "available-products".to_owned();
                let request = Some(&input_scenes);
                (endpoint, request)
            }
        };

        let response = self.get(&endpoint, request).await?;
        let str_response = to_string(&response)?;
        match response {
            ApiResponse::AvailableProducts(products) => Ok(products),
            _ => Err(Error::UnexpectedResponse {
                response: str_response,
                url: endpoint.to_owned(),
                source: None,
            }),
        }
    }

    /// Perform a GET request
    pub(crate) async fn get<S: Serialize>(
        &self,
        endpoint: &str,
        body: Option<&S>,
    ) -> Result<ApiResponse> {
        let call_url = self.base_url.join(endpoint)?;
        let str_url = call_url.to_string();
        let response = self
            .client
            .get(call_url)
            .basic_auth(&self.credentials.username, Some(&self.credentials.password))
            .json(&body)
            .send()
            .await?
            .text()
            .await?;

        let json_response = match from_str::<ApiResponse>(&response) {
            Ok(value) => value,
            Err(e) => {
                return Err(Error::UnexpectedResponse {
                    response: response,
                    url: str_url,
                    source: Some(e),
                })
            }
        };

        Ok(json_response)
    }

    /// Perform a POST request
    pub(crate) async fn post<S: Serialize>(
        &self,
        endpoint: &str,
        body: Option<&S>,
    ) -> Result<ApiResponse> {
        let call_url = self.base_url.join(endpoint)?;
        let str_url = call_url.to_string();
        let response = self
            .client
            .post(call_url)
            .basic_auth(&self.credentials.username, Some(&self.credentials.password))
            .json(&body)
            .send()
            .await?
            .text()
            .await?;

        let json_response = match from_str::<ApiResponse>(&response) {
            Ok(value) => value,
            Err(e) => {
                return Err(Error::UnexpectedResponse {
                    response: response,
                    url: str_url,
                    source: Some(e),
                })
            }
        };

        Ok(json_response)
    }
}

// Don't try to move tests to their own module. This will make them into
// integration tests and invalidate the #[cfg(test)] compile flag used
// to use the mockito server URL. E.g., it will actually try to call
// the actual API URL when running `cargo test`.USGS_API_URL
//
// See also the relevant issue: https://github.com/rust-lang/rust/issues/45599
#[cfg(test)]
mod test {
    use crate::types::{
        AvailableProducts, Credentials, FormatKind, Formats, ItemStatus, OrderDefinition,
        OrderDetails, OrderStatus, PostOrderRequest, ProductOpt, User,
    };
    use crate::Client;
    use std::collections::HashMap;
    use std::fs::read_to_string;

    use mockito::mock;

    #[tokio::test]
    async fn test_nominal() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials {
            username: "alice_p_hacker".to_owned(),
            password: "pass123".to_owned(),
        };

        let client = Client::new(credentials).unwrap();
        let _user: User = client.user().await.unwrap();
        let _formats: Formats = client.formats().await.unwrap();
        let orders: Vec<String> = client.list_orders().await.unwrap();
        let _status: OrderStatus = client.order_status(&orders[0]).await.unwrap();
        let _details: OrderDetails = client.get_order(&orders[0]).await.unwrap();
    }

    #[tokio::test]
    async fn test_item_status() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials {
            username: "alice_p_hacker".to_owned(),
            password: "pass123".to_owned(),
        };

        let client = Client::new(credentials).unwrap();
        let orders: Vec<String> = client.list_orders().await.unwrap();
        let _status: ItemStatus = client.item_status(&orders[0], None).await.unwrap();
        let _single_item_status: ItemStatus = client
            .item_status(&orders[0], Some("LC08_L1TP_197018_20181023_20181031_01_T1"))
            .await
            .unwrap();
    }

    #[tokio::test]
    async fn test_available_products() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials {
            username: "alice_p_hacker".to_owned(),
            password: "pass123".to_owned(),
        };

        let client = Client::new(credentials).unwrap();
        let scenes = vec!["LE07_L1TP_029030_20170221_20170319_01_T1".to_owned()];
        let _available_products_single: AvailableProducts =
            client.available_products(scenes).await.unwrap();

        let scenes = vec![
            "LE07_L1TP_029030_20170221_20170319_01_T1".to_owned(),
            "MOD09A1.A2000073.h12v11.005.2008238080250.hdf".to_owned(),
            "bad_scene_id".to_owned(),
        ];
        let _available_products_many: AvailableProducts =
            client.available_products(scenes).await.unwrap();
    }

    #[tokio::test]
    async fn test_post_order() {
        let _mock_server = mock_server_setup();
        let credentials = Credentials {
            username: "alice_p_hacker".to_owned(),
            password: "pass123".to_owned(),
        };

        let client = Client::new(credentials).unwrap();
        let products = vec!["sr".to_owned(), "toa".to_owned()];
        let scenes_l8 = vec![
            "LC08_L1TP_197018_20181023_20181031_01_T1".to_owned(),
            "LC08_L1TP_197019_20181023_20181031_01_T1".to_owned(),
            "LC08_L1TP_197018_20181007_20181029_01_T1".to_owned(),
        ];
        let scenes_l7 = vec![
            "LE07_L1TP_196019_20181024_20181119_01_T1".to_owned(),
            "LE07_L1TP_198018_20181022_20181119_01_T1".to_owned(),
            "LE07_L1TP_198019_20181022_20181119_01_T1".to_owned(),
        ];
        let mut order_request: PostOrderRequest = HashMap::new();
        order_request.insert(
            "olitirs8_collection".to_owned(),
            ProductOpt::OrderDefinition(OrderDefinition {
                inputs: scenes_l8,
                products: products.clone(),
            }),
        );
        order_request.insert(
            "etm7_collection".to_owned(),
            ProductOpt::OrderDefinition(OrderDefinition {
                inputs: scenes_l7,
                products: products.clone(),
            }),
        );
        order_request.insert("format".to_owned(), ProductOpt::Format(FormatKind::Gtiff));

        use serde_json::to_string_pretty;
        println!("{}", to_string_pretty(&order_request).unwrap());
        let _post_reponse = client.post_order(order_request).await.unwrap();
    }

    fn mock_server_setup() -> Vec<mockito::Mock> {
        let mut available_products_request =
            read_to_string("test-data/available-products-request.json").unwrap();
        available_products_request.retain(|c| c != ' ');
        available_products_request.retain(|c| c != '\n');

        let user = mock("GET", "/user")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .with_body_from_file("test-data/user-response.json")
            .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
            .create();

        let formats = mock("GET", "/formats")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .with_body_from_file("test-data/formats-response.json")
            .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
            .create();

        let list_orders = mock("GET", "/list-orders")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .with_body_from_file("test-data/list-orders-response.json")
            .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
            .create();

        let order_status = mock(
            "GET",
            "/order-status/espa-alice@stcorp.no-09282020-152613-506",
        )
        .with_status(200)
        .with_header("Content-Type", "application/json")
        .with_body_from_file("test-data/order-status-response.json")
        .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
        .create();

        let order_details = mock("GET", "/order/espa-alice@stcorp.no-09282020-152613-506")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .with_body_from_file("test-data/order-details-response.json")
            .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
            .create();

        let item_status = mock(
            "GET",
            "/item-status/espa-alice@stcorp.no-09282020-152613-506",
        )
        .with_status(200)
        .with_header("Content-Type", "application/json")
        .with_body_from_file("test-data/item-status-response.json")
        .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
        .create();

        let single_item_status = mock("GET", "/item-status/espa-alice@stcorp.no-09282020-152613-506/LC08_L1TP_197018_20181023_20181031_01_T1")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .with_body_from_file("test-data/item-status-response.json")
            .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
            .create();

        let available_products_single = mock(
            "GET",
            "/available-products/LE07_L1TP_029030_20170221_20170319_01_T1",
        )
        .with_status(200)
        .with_header("Content-Type", "application/json")
        .with_body_from_file("test-data/available-products-single-response.json")
        .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
        .create();

        let available_products_many = mock("GET", "/available-products")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .with_body_from_file("test-data/available-products-many-response.json")
            .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
            .match_body(available_products_request.as_str())
            .create();

        let post_order = mock("POST", "/order")
            .with_status(200)
            .with_header("Content-Type", "application/json")
            .with_body_from_file("test-data/post-order-response.json")
            .match_header("authorization", "Basic YWxpY2VfcF9oYWNrZXI6cGFzczEyMw==")
            .create();

        return vec![
            user,
            formats,
            list_orders,
            order_status,
            order_details,
            item_status,
            single_item_status,
            available_products_single,
            available_products_many,
            post_order,
        ];
    }
}
